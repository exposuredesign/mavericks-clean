<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mavericks_clean');

/** MySQL database username */
define('DB_USER', 'forge'); /*homestead*//*maverick2_nhfx*/

/** MySQL database password */
define('DB_PASSWORD', 'wxEWsGhjIvQInQmzwy5b'); /*secret*//*nvupmcuqv8e4unm8*/

/** MySQL hostname */
define('DB_HOST', '46.101.17.208'); /*localhost*//*10.169.0.118*/

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '
	1JuX(*~~5Q@&AoM>An%Vri%d^={dZrznyIRXhe,sQWRDbdo%$Ff%SXk}cgMD<7D)');
define('SECURE_AUTH_KEY',  '
	@3usV`bK-8=L]j[pp6(Em=p~SRG7W-BN@OEBufIKP7ly9>;>hp=-fApsO(-p#H7m');
define('LOGGED_IN_KEY',    '
	5@B0R7R,!~bJsF#&ou)bl#0e!CtlT]darRZ;XRbnpL^mj?0-7dQN/G.Zf&bb7j(1');
define('NONCE_KEY',        '
	j[4zB`ZBh}G4@7_W~+(ms8);7-Je |P;:f,B{DZl[#1s?XN_+]Fh_*4a|1<F?Y0w');
define('AUTH_SALT',        '
	CCm0a+Ra`c}sBjl`O[;RNLWy._t:E._).$ZC8 @oxS*&!qzLv~uOUzsE&}+C)iiz');
define('SECURE_AUTH_SALT', '
	*,?.H|,Pl5Xo|%A17r|;)R fJM=UTr_yGv+T?k]oh_pSGaqgoZpvdK)aJPZS`AbA');
define('LOGGED_IN_SALT',   '
	6eCvz^Y8plBh<sLr,ZdyGh$SnVgigk(z7bsZ~k7*/n`{%[|oeZ{|c+H>-U|g*fU8');
define('NONCE_SALT',       '
	oYr0~yG2T41v%.aN-Z4sA^(>1<MB6t R5FwyH(.ykp(V-v3&f;h<5aUOP/`C340t');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
